package naist.cl.shmm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import naist.cl.util.MyReader;
import naist.cl.util.Pair;

public class FeatureExtractor {

	public static ArrayList<String> extractor(Model model, String sentence,
			int maxLen, boolean type) {
		ArrayList<String> featureList = new ArrayList();
		for (int i = 0; i < sentence.length(); i++) {
			featureList.addAll(commonFeatureParse(model, sentence, i, maxLen,
					type));
		}
		return featureList;
	}

	private static ArrayList<String> nodeFeatureParse(Model model,
			String sentence, int begin, int length, boolean type) {
		ArrayList<String> featureList = new ArrayList();

		String word = sentence.substring(begin, begin + length);
		// // word feature
		featureList.add("w[0]=" + word);
		featureList.add("#w[0]#=" + length);

		// unigram character feature, word left and right character
		featureList.add("wcl[" + (-1) + "]="
				+ commonFeatureParse(sentence, begin - 1));
		featureList.add("wcl[" + (-2) + "]="
				+ commonFeatureParse(sentence, begin - 2));
		featureList.add("wcr[" + 1 + "]="
				+ commonFeatureParse(sentence, begin + length));
		featureList.add("wcr[" + 2 + "]="
				+ commonFeatureParse(sentence, begin + length + 1));

		// // bigram feature; left character + word_begin_character
		featureList.add("wcl[" + (-1) + "][" + 0 + "]="
				+ commonFeatureParse(sentence, begin - 1) + word.charAt(0));
		featureList.add("wcl[" + (-2) + "][" + (-1) + "]="
				+ commonFeatureParse(sentence, begin - 2)
				+ commonFeatureParse(sentence, begin - 1));
		// // bigram feature; right character + word_last_character
		featureList.add("wcr[" + (-1) + "][" + 1 + "]="
				+ word.charAt(length - 1)
				+ commonFeatureParse(sentence, length + begin));
		featureList.add("wcr[" + (-1) + "][" + 2 + "]="
				+ word.charAt(length - 1)
				+ commonFeatureParse(sentence, length + begin + 1));

		// word_begin_character + word_last_character
		featureList.add("wc[" + 0 + "][" + -1 + "]=" + word.charAt(0)
				+ word.charAt(word.length() - 1));

		// characters in_word
		for (int j = 0; j < word.length(); j++) {
			featureList.add("wc[" + j + "]=" + word.charAt(j));
			// inernal bigram
			if (j + 1 < word.length())
				featureList.add("wc[" + j + "][" + (j + 1) + "]="
						+ word.charAt(j));
		}
		addDictionaryFeature(model, word);

		if (type)
			featureList.addAll(typeFeature(sentence, begin, length));
		if (null != model.getAvMap() && !model.getAvMap().isEmpty())
			featureList.addAll(addAVfeature(model, sentence, begin, length));
		if (null != model.getLDAMap() && !model.getLDAMap().isEmpty())
			featureList.addAll(addLDAFeature(model, sentence, begin, length));

		if (null != model.getBrownMap() && !model.getBrownMap().isEmpty())
			featureList.addAll(addBrownFeature(model, sentence, begin, length));

		// if (null != model.getNNLMMap() && !model.getNNLMMap().isEmpty())
		// featureList.addAll(addNNLMFeature(model, sentence, begin, length));
		if (null != model.getNNLMMap() && !model.getNNLMMap().isEmpty())
			featureList.addAll(addNNLMWordFeature(model, sentence, begin,
					length));
		// System.out.println(featureList);
		return featureList;

	}

	private static ArrayList<String> addDictionaryFeature(Model model,
			String word) {
		ArrayList<String> featureList = new ArrayList();
		if (null != model.getInnterDictionary()
				&& !model.getInnterDictionary().isEmpty()
				&& model.getInnterDictionary().containsKey(word)
				&& model.getInnterDictionary().get(word) > 2) {
			featureList.add("w[0]=#1#");
		}
		return featureList;
	}

	private static ArrayList<String> addAVfeature(Model model, String sentence,
			int begin, int length) {
		ArrayList<String> featureList = new ArrayList();
		int b = Math.max(0, begin - 1);
		int e = Math.min(sentence.length(), begin + length + 1);

		for (int j = b; j < e; j++) {
			String c = sentence.charAt(j) + "";
			if (model.getAvMap().get(c) != null) {
				int bp = Math.min(model.getAvMap().get(c).getValue(), model
						.getAvMap().get(c).getKey());
				if (j >= begin + length) {
					featureList.add("avr[" + (j - begin - length) + "]=" + bp);

				} else {
					featureList.add("av[" + (j - begin) + "]=" + bp);
				}
			}
		}

		return featureList;

	}

	private static ArrayList<String> addLDAFeature(Model model,
			String sentence, int begin, int length) {
		ArrayList<String> featureList = new ArrayList();
		for (int j = begin; j < length; j++) {
			String c = sentence.charAt(j) + "";
			if (model.getLDAMap().get(c) != null) {
				Vector<Double> vec = model.getLDAMap().get(c);
				for (int i = 0; i < vec.size(); i++)
					if (vec.get(i) != 0)
						featureList.add("ldal[" + j + "][" + i + "]" + ":#="
								+ vec.get(i) * 0.01);
			}

		}
		return featureList;
	}

	private static ArrayList<String> addNNLMFeature(Model model,
			String sentence, int begin, int length) {
		ArrayList<String> featureList = new ArrayList();

		for (int j = begin; j < length; j++) {
			String c = sentence.charAt(j) + "";

			if (model.getNNLMMap().get(c) != null) {
				Vector<Double> vec = model.getNNLMMap().get(c);
				for (int i = 0; i < vec.size(); i++)
					if (vec.get(i) != 0)
						featureList.add("nnlm[" + j + "][" + i + "]" + ":#="
								+ vec.get(i) * 0.01);
			}

		}
		return featureList;
	}

	private static ArrayList<String> addNNLMWordFeature(Model model,
			String sentence, int begin, int length) {
		ArrayList<String> featureList = new ArrayList();
		if (null != model.getNNLMMap() && !model.getNNLMMap().isEmpty()) {
			String word = "";
			Vector<Double> vec = null;
			word = sentence.substring(begin, begin + length);
			if (model.getNNLMMap().get(word) != null) {
				vec = model.getNNLMMap().get(word);
				for (int i = 0; i < vec.size(); i++)
					if (vec.get(i) != 0)
						featureList.add("nnlmw[-1][" + i + "]" + ":#="
								+ vec.get(i) * 0.01);
			}
		}
		return featureList;
	}

	//

	// add brown features
	private static ArrayList<String> addBrownFeature(Model model,
			String sentence, int begin, int length) {
		ArrayList<String> featureList = new ArrayList();
		int b = Math.max(0, begin - 1);
		int e = Math.min(sentence.length(), begin + length + 1);

		for (int j = b; j < e; j++) {
			String c = sentence.charAt(j) + "";
			if (model.getBrownMap().get(c) != null) {
				String bp = model.getBrownMap().get(c).getKey();
				if (j >= begin + length) {
					featureList.add("brownr[" + (j - begin - length) + "]="
							+ bp.substring(0, Math.min(3, bp.length())));
					featureList.add("brownrFull[" + (j - begin - length) + "]="
							+ bp);
				} else {
					featureList.add("brown[" + (j - begin) + "]="
							+ bp.substring(0, Math.min(3, bp.length())));
					featureList.add("brownFull[" + (j - begin) + "]=" + bp);
				}
			}
		}

		return featureList;
	}

	// for node feature extraction in both training and word segmentation
	public static int[] nodeFeatureParseForLattice(Model model,
			HashMap<Integer, Double> cw, String sentence, int begin, int length) {
		HashMap<Integer, Boolean> map = new HashMap();
		ArrayList<String> featureList = nodeFeatureParse(model, sentence,
				begin, length, model.getType());

		// System.out.println(sentence);
		for (String ft : featureList) {
			if (ft.contains(":#=")) {
				int idx = ft.indexOf(":#=");
				double w = Double.parseDouble(ft.substring(idx + 3));
				ft = ft.substring(0, idx);
				if (model.getFeatureMap().containsKey(ft)) {
					int fid = model.getFeatureMap().get(ft);
					if (!map.containsKey(fid)) {
						map.put(fid, true);
					}
					if (!cw.containsKey(fid)) {
						cw.put(fid, w);
					}
				}
				continue;
			}
			if (model.getFeatureMap().containsKey(ft)) {
				int fid = model.getFeatureMap().get(ft);
				if (!map.containsKey(fid)) {
					map.put(fid, true);
				}
			}
		}
		int[] farray = new int[map.size()];
		Iterator<Integer> it = map.keySet().iterator();
		int index = 0;
		while (it.hasNext()) {
			farray[index] = it.next();
			index++;
		}
		return farray;
	}

	//
	private static ArrayList<String> commonFeatureParse(Model model,
			String sentence, int pos, int maxLen, boolean type) {
		ArrayList<String> featureList = new ArrayList();

		int max;
		if (maxLen == -1)
			max = sentence.length();
		else
			max = Math.min(pos + maxLen, sentence.length());
		for (int i = pos; i < max; i++) {
			String word = sentence.substring(pos, i + 1);
			featureList.addAll(nodeFeatureParse(model, sentence, pos,
					word.length(), type));
		}
		return featureList;
	}

	// todo list
	private static ArrayList<String> typeFeature(String sentence, int begin,
			int length) {
		ArrayList<String> featureList = new ArrayList();
		for (int i = begin; i < begin + length; i++) {
			if (CharacterType.type(sentence.charAt(i) + "") >= 0)
				featureList.add("wt[" + (i - begin) + "]="
						+ CharacterType.type(sentence.charAt(i) + ""));
		}
		if (CharacterType.StringType(sentence.substring(begin, begin + length)) > 0) {
			featureList.add("swt[ft]="
					+ CharacterType.StringType(sentence.substring(begin, begin
							+ length)));
		}
		return featureList;
	}

	private static String commonFeatureParse(String sentence, int i) {
		if (i < 0)
			return DummyFeature.BOS[-1 - i];
		else if (i >= sentence.length())
			return DummyFeature.EOS[i - sentence.length()];
		else
			return sentence.charAt(i) + "";

	}

	// extract training file features

	public static void featuresExtractor(Model model, Setting setting)
			throws IOException {
		MyReader reader = new MyReader(setting.tfile);
		HashMap<String, Pair<Integer, Integer>> featureMap = new HashMap();
		HashMap<Integer, Double> myweights = new HashMap();
		String line;

		int index = 0;
		while (null != (line = reader.readLine())) {
			line = line.trim();

			if (line.isEmpty())
				continue;
			line = CharacterType.convert2Half(line);
			ArrayList<String> featureList = FeatureExtractor.extractor(model,
					line.replaceAll("\\s+", ""), setting.maxLen, setting.type);
			if (null == featureList)
				continue;
			for (String feature : featureList) {
				if (feature.contains(":#=")) {
					int idx = feature.indexOf(":#=");
					feature = feature.substring(0, idx);
				}
				if (featureMap.containsKey(feature))
					featureMap.get(feature).setValue(
							featureMap.get(feature).getValue() + 1);
				else {
					featureMap.put(feature, new Pair(featureMap.size(), 1));
				}
			}

			// System.out.println(index+":"+featureMap.size());
			index++;
		}
		model.setFeatureMap(filter(featureMap, setting.freq));
	}

	public static HashMap<String, Integer> ditcitonaryExtract(String tfile)
			throws IOException {
		MyReader reader = new MyReader(tfile);
		HashMap<String, Integer> dictionary = new HashMap();
		String line;
		while (null != (line = reader.readLine())) {
			line = line.trim();
			if (line.isEmpty())
				continue;
			String[] words = line.split("\\s+");

			for (String word : words) {
				if (dictionary.containsKey(word))
					dictionary.put(word, dictionary.get(word) + 1);
				else
					dictionary.put(word, 1);
			}

		}
		return dictionary;
	}

	public static HashMap<String, Integer> filter(
			HashMap<String, Pair<Integer, Integer>> featureMap, int freq) {
		HashMap<String, Integer> map = new HashMap();
		Iterator<String> it = featureMap.keySet().iterator();
		int id = 0;
		while (it.hasNext()) {
			String ft = it.next();
			Pair<Integer, Integer> pair = featureMap.get(ft);
			if (pair.getValue() > freq) {
				map.put(ft, id++);
			}
		}
		featureMap.clear();
		return map;
	}
}
