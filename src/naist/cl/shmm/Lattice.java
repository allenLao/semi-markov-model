package naist.cl.shmm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

public class Lattice {
	private Vector<Vector<Node>> ltt;
	private String sentence;
	private String gold;
	private boolean trainingStep;
	private LinkedList<Node> goldList;
	private int maxLength;

	public Lattice(String sentence, Model model, int maxLength,
			boolean trainingStep) {	
		this.trainingStep = trainingStep;
		if (trainingStep) {
			gold = sentence;
			this.sentence = sentence.replaceAll("\\s+", "");
		} else {
			this.sentence = sentence;
		}
		this.maxLength = maxLength > 0 ? maxLength : this.sentence.length();
		ltt = new Vector();
		buildLattice(model);
		if (trainingStep)
			parseGold(model);
	}

	public double getNumWord() {
		return goldList.size();
	}

	public LinkedList<Node> decoding(Model model) {
		// init weight
		for (int i = 0; i < ltt.size(); i++) {
			for (int j = 0; j < ltt.get(i).size(); j++) {
				Node node = ltt.get(i).get(j);
				node.setBest(null);
				node.setScore(0);
				if (null != node.getCWeight()) {
					for (int m = 0; m < node.getFeatureVector().length; m++) {
						int fid = node.getFeatureVector()[m];
						if (node.getCWeight().containsKey(fid)) {
							node.setScore(node.getScore()
									+ model.lookUpFeatureWeight(fid)
									* node.getCWeight().get(fid));
							continue;
						} else {

							node.setScore(node.getScore()
									+ model.lookUpFeatureWeight(fid));
						}
					}

				} else
					for (int m = 0; m < node.getFeatureVector().length; m++) {
						node.setScore(node.getScore()
								+ model.lookUpFeatureWeight(node
										.getFeatureVector()[m]));
					}
			}
		}
		// forward
		for (int i = 0; i < ltt.size(); i++) {
			for (int j = 0; j < ltt.get(i).size(); j++) {
				Node curr = ltt.get(i).get(j);
				Vector<Node> lefts = backwordNodes(curr.getBegin());
				if (null == lefts || lefts.isEmpty()) {
					curr.setLeftScores(curr.getScore());
					curr.setBest(null);
				} else {
					Node best = lefts.get(0);
					double max = best.getLeftScores();
					for (int m = 1; m < lefts.size(); m++) {
						if (lefts.get(m).getLeftScores() > max) {
							best = lefts.get(m);
							max = best.getLeftScores();
						}
					}
					curr.setLeftScores(curr.getScore() + best.getLeftScores());
					curr.setBest(best);
				}
				// System.out.println(sentence.substring(curr.getBegin(),
				// curr.getBegin() + curr.getLength())
				// + curr.getLeftScores());
			}
		}

		Node best = ltt.get(ltt.size() - 1).get(0);
		for (int i = 1; i < ltt.get(ltt.size() - 1).size(); i++) {
			if (best.getLeftScores() < ltt.get(ltt.size() - 1).get(i)
					.getLeftScores()) {
				best = ltt.get(ltt.size() - 1).get(i);
			}
		}
		LinkedList<Node> bestPath = new LinkedList();
		while (null != best) {
			bestPath.addFirst(best);
			best = best.getBest();
		}
		return bestPath;
	}

	public LinkedList<Node> getGoldList() {
		return goldList;
	}

	public int row() {
		return ltt.size();
	}

	// add correct path
	private void parseGold(Model model) {
		goldList = new LinkedList();
		String[] words = gold.trim().split("\\s+");
		int begin = 0;
		for (int i = 0; i < words.length; i++) {
			// System.out.println(words[i]);
			int len = words[i].length();
			int index = len + begin - 1;
			if (len > maxLength) {
				HashMap<Integer, Double> cw = new HashMap();
				Node node = new Node();
				node.setBegin(begin);
				node.setLength(len);
				node.setFeatuerVector(FeatureExtractor
						.nodeFeatureParseForLattice(model, cw, sentence, begin,
								len));
				if (!cw.isEmpty()) {
					node.setCWeight(cw);
					// System.out.println(cw.size());
				}
				ltt.get(index).add(node);
				goldList.add(node);
			} else {
				goldList.add(ltt.get(index).get(len - 1));
			}
			begin += len;
		}
	}

	public int column(int row) {
		if (row > row() || row < 0)
			return -1;
		else
			return ltt.get(row).size();
	}

	public String getSentence() {
		return sentence;
	}

	public void buildLattice(Model model) {
		ltt = new Vector<Vector<Node>>();
		for (int i = 0; i < sentence.length(); i++) {
			Vector<Node> vnode = new Vector<Node>();
			// how many node
			int numNode;
			if (i < maxLength) {
				numNode = i + 1;
			} else {
				numNode = maxLength;
			}
			
			// System.out.println(numNode + "~~~~~~");
			for (int k = 0; k < numNode; k++) {
				int begin = i - k;
				int length = k + 1;
				Node node = new Node();
				HashMap<Integer, Double> cw = new HashMap();
				node.setBegin(begin);
				node.setLength(length);
				node.setFeatuerVector(FeatureExtractor
						.nodeFeatureParseForLattice(model, cw, CharacterType.convert2Half(sentence), begin,
								length));
				if (!cw.isEmpty()) {
					node.setCWeight(cw);
					// System.out.println(cw.size());
				}
				// System.out.println(begin + "," + length + ","
				// + sentence.substring(begin, begin + length) + ","
				// + node.getFeatureVector().length);

				vnode.add(node);
			}
			// System.out.println(vnode.size());
			ltt.add(vnode);
		}

	}

	public Node getNode(int row, int column) {
		if (row < 0 || row > row() || column(row) < 0)
			return null;
		return ltt.get(row).get(column);
	}

	public Vector<Node> backwordNodes(int begin) {
		int left = begin - 1;
		if (left < 0)
			return null;
		else
			return ltt.get(left);
	}

	public Vector<Node> forwardNodes(int begin, int length, int maxLength) {
		int size = ltt.size();
		int numNode = size - length - begin;

		if (numNode > maxLength)
			numNode = maxLength;
		else if (numNode <= 0)
			numNode = 0;
		Vector<Node> right = new Vector<Node>();
		for (int i = 0; i < numNode; i++) {
			if (getNode(begin + length + i, i) != null)
				right.add(getNode(begin + length + i, i));
		}
		return right;
	}

	public Vector<Node> getRow(int pos) {
		return ltt.get(pos);
	}

}
