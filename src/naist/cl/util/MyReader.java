package naist.cl.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;


public class MyReader {
	private BufferedReader reader;
	private int counter;

	public MyReader(String fileName) throws IOException {
		reader = new BufferedReader(new FileReader(fileName));
		counter = 0;
	}

	public String readLine() throws IOException {
		counter += 1;
		return reader.readLine();
	}

	public int getCounter() {
		return counter;
	}

	public void close() throws IOException {
		reader.close();
	}

	public static HashMap<String, Pair<Integer, Integer>> parseAv(String av) {
		HashMap<String, Pair<Integer, Integer>> avMap = new HashMap<String, Pair<Integer, Integer>>();
		try {
			MyReader reader = new MyReader(av);
			String line;
			while (null != (line = reader.readLine())) {
				if (line.isEmpty())
					continue;
				String[] terms = line.split("\\s+");
				avMap.put(terms[0], new Pair(Integer.parseInt(terms[1]),
						Integer.parseInt(terms[2])));
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		return avMap;
	}

	public static HashMap<String, Pair<String, Integer>> parseBrown(String brown) {
		HashMap<String, Pair<String, Integer>> brownMap = new HashMap<String, Pair<String, Integer>>();
		try {
			MyReader reader = new MyReader(brown);
			String line;
			while (null != (line = reader.readLine())) {
				if (line.isEmpty())
					continue;
				String[] terms = line.split("\\s+");
				brownMap.put(terms[1],
						new Pair(terms[0], Integer.parseInt(terms[2])));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.exit(0);
			e.printStackTrace();
		}
		return brownMap;
	}

	public static HashMap<String, Vector<Double>> parseNNLM(String nnlm) {
		HashMap<String, Vector<Double>> nnlmMap = new HashMap<String, Vector<Double>>();
		try {
			MyReader reader = new MyReader(nnlm);
			String line;
			while (null != (line = reader.readLine())) {
				if (line.isEmpty())
					continue;
				String[] terms = line.split("\\s+");
				Vector<Double> vec = new Vector();
				for (int i = 1; i < terms.length; i++)
					vec.add(Double.parseDouble(terms[i]));
				nnlmMap.put(terms[0], vec);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.exit(0);
			e.printStackTrace();
		}
		return nnlmMap;
	}

	public static HashMap<String, Vector<Double>> parseLDA(String lda) {
		HashMap<String, Vector<Double>> ldaMap = new HashMap<String, Vector<Double>>();
		try {
			MyReader reader = new MyReader(lda);
			String line;
			while (null != (line = reader.readLine())) {
				if (line.isEmpty())
					continue;
				String[] terms = line.split("\\s+");
				Vector<Double> vec = new Vector();
				for (int i = 1; i < terms.length; i++)
					vec.add(Double.parseDouble(terms[i]));
				ldaMap.put(terms[0], vec);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.exit(0);
			e.printStackTrace();
		}
		return ldaMap;
	}
}