package naist.cl.util;

//command line setting
public class CommandSetting {
	public static final String ACTION = "action";
	public static final String TRANININGFILE = "tfile";
	public static final String MAXITERATION = "maxIter";
	public static final String CORRECTRATE = "crate";
	public static final String MAXLENGTH = "maxLen";
	public static final String DICTIONNARYFEATURE = "dicFt";
	public static final String MODEL = "model";
	public static final String OUPUT = "output";
	public static final String INPUT = "input";
	public static final String FILTERFREQ = "freq";
	public static final String HELP = "help";
	public static final String BROWN = "brown";
	public static final String NNLM = "nnlm";
	public static final String AV = "av";
	public static final String LDA = "lda";
	public static final String NNLM_SCALAR = "nscalar";
	public static final String LDA_SCALAR = "lcalar";
	public static final String CACHE = "cache";
	public static final String TYPE = "type";
	public static final String TOKEN_TYPE = "tokenType";
}