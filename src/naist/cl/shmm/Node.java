package naist.cl.shmm;

import java.util.HashMap;

public class Node {
	private HashMap<Integer, Double> cweights = null;
	private int begin;
	private int length;
	private int[] fvect;
	private double score;
	private double lscore;
	private Node best; // keep the best path
	private int tag;

	public Node() {

	}

	public void setCWeight(HashMap<Integer, Double> cweights) {
		this.cweights = cweights;
	}

	public HashMap<Integer, Double> getCWeight() {
		return cweights;
	}

	public void setBest(Node best) {
		this.best = best;
	}

	public Node getBest() {
		return best;
	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getTag() {
		return tag;
	}

	public boolean isCorrect() {
		return false;
	}

	public int getBegin() {
		return begin;
	}

	public int getLength() {
		return length;
	}

	public double getScore() {
		return score;
	}

	public double getLeftScores() {
		return lscore;
	}

	public void setBegin(int i) {
		this.begin = i;
	}

	public void setLength(int len) {
		this.length = len;
	}

	public void setLeftScores(double lscore) {
		this.lscore = lscore;
	}

	public void setFeatuerVector(int[] fvect) {
		this.fvect = fvect;
	}

	public int[] getFeatureVector() {
		return fvect;
	}

	public void setScore(double score) {
		this.score = score;
	}

}