package naist.cl.shmm;

public class DummyFeature {
	public static final String[] BOS = { "BOS_0", "BOS_1", "BOS_2", "BOS_3",
			"BOS_4" };
	public static final String[] EOS = { "EOS0", "EOS1", "EOS2", "EOS3", "EOS4" };
}