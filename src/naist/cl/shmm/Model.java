package naist.cl.shmm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import naist.cl.util.MyReader;
import naist.cl.util.Pair;


public class Model implements Serializable {
	private HashMap<String, Integer> featureMap;
	private HashMap<String, Integer> innerDictionary;
	private HashMap<String, Pair<String, Integer>> brownMap;
	private HashMap<String, Pair<Integer, Integer>> avMap;
	private HashMap<String, Vector<Double>> ldaMap;
	private HashMap<String, Vector<Double>> nnlmMap;
	private HashMap<Integer, Double> userWeights;
	private double[] weights;
	private double[] averageWeights;
	private boolean type;

	public Model(HashMap<String, Integer> featureMap) {
		this.featureMap = featureMap;
		this.innerDictionary = null;
		initmodel();
	}

	public Model() {
		this.featureMap = null;
		this.innerDictionary = null;
		this.brownMap = null;
		this.avMap = null;
		this.ldaMap = null;
		this.nnlmMap = null;
	}

	public Model(HashMap<String, Integer> featureMap, double[] weights,
			HashMap<String, Integer> innerDictionary) {
		this.featureMap = featureMap;
		this.weights = weights;
		this.innerDictionary = innerDictionary;
	}

	public Model(HashMap<String, Integer> featureMap, double[] weights) {
		this.featureMap = featureMap;
		this.weights = weights;
		this.innerDictionary = null;
	}

	public void setUserWeights(HashMap<Integer, Double> userWeights) {
		this.userWeights = userWeights;
	}

	public HashMap<Integer, Double> getUserWeight() {
		return userWeights;
	}

	public void setAvMap(HashMap<String, Pair<Integer, Integer>> avMap) {
		this.avMap = avMap;
	}

	public HashMap<String, Pair<Integer, Integer>> getAvMap() {
		return avMap;
	}

	public void setBrownMap(HashMap<String, Pair<String, Integer>> brown) {
		this.brownMap = brown;
	}

	public HashMap<String, Pair<String, Integer>> getBrownMap() {
		return brownMap;
	}

	public void setLDAMap(HashMap<String, Vector<Double>> lda) {
		this.ldaMap = lda;
	}

	public HashMap<String, Vector<Double>> getLDAMap() {
		return ldaMap;
	}

	public void setNNLMMap(HashMap<String, Vector<Double>> nnlm) {
		this.nnlmMap = nnlm;
	}

	public HashMap<String, Vector<Double>> getNNLMMap() {
		return nnlmMap;
	}

	public void setInnerDictionary(HashMap<String, Integer> innerDictionary) {
		this.innerDictionary = innerDictionary;
	}

	public HashMap<String, Integer> getInnterDictionary() {
		return innerDictionary;
	}

	public int wordFreqDictionary(String word) {
		int freq = -1;
		if (innerDictionary.containsKey(word)) {
			freq = innerDictionary.get(word);
		}
		return freq;
	}

	public void setType(boolean type) {
		this.type = type;
	}

	public boolean getType() {
		return type;
	}

	public HashMap<String, Integer> getFeatureMap() {
		return featureMap;
	}

	public void initmodel() {
		weights = new double[featureMap.size()];
		averageWeights = new double[featureMap.size()];
	}

	public double lookUpFeatureWeight(int fid) {
		return weights[fid];
	}

	public void setFeatureMap(HashMap<String, Integer> featureMap) {
		this.featureMap = featureMap;
		initmodel();
	}

	// public void setMaxLength(int max) {
	// this.maxLength = max;
	// }

	public void updateAveWeight(int fid, double value) {
		averageWeights[fid] += value;
	}

	public void updateWeight(int fid, double value) {
		weights[fid] += value;
	}

	public void modelWeightUpdate(double c) {
		for (int i = 0; i < averageWeights.length; i++) {
			averageWeights[i] = weights[i] - averageWeights[i] / c;
		}
	}

	public static Model readModel(String path) {
		Model model = null;
		try {
			MyReader reader = new MyReader(path);
			String line = reader.readLine();
			// read features
			boolean mtype = Boolean.parseBoolean(line.split(":")[1]);
			line = reader.readLine();
			if (null == line) {
				System.err.println("please input an model file~~");
				return null;
			} else if (!line.split(":")[0].equalsIgnoreCase("FEATURESIZE")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
				System.err.println("please input an model file~~");
				return null;
			}

			int featureSize = Integer.parseInt(line.split(":")[1]);
			HashMap<String, Integer> fmap = new HashMap<String, Integer>();
			double[] weights = new double[featureSize];
			for (int i = 0; i < featureSize; i++) {
				line = reader.readLine();
				String feature = line.split("\\s+")[0];
				double value = Double.parseDouble(line.split("\\s+")[1]);
				fmap.put(feature, i);
				weights[i] = value;
				model = new Model(fmap, weights);
				// System.out.println(fmap.get(feature) + " " + weights[i]);
			}
			if (model == null) {
				System.err.println("read feature map errors");
				return null;
			}
			// read dictionaries if any
			reader.readLine(); // read an empty line
			line = reader.readLine();
			if (line.split(":")[0].equalsIgnoreCase("DICTIONARY")
					&& Integer.parseInt(line.split(":")[1]) > 1) {
				int dicSize = Integer.parseInt(line.split(":")[1]);
				HashMap<String, Integer> dictionary = new HashMap<String, Integer>();

				// read features
				for (int i = 0; i < dicSize; i++) {
					line = reader.readLine();
					String feature = line.split("\\s+")[0];
					int value = Integer.parseInt(line.split("\\s+")[1]);
					dictionary.put(feature, value);
				}
				System.out.println(dictionary.size());
				model.setInnerDictionary(dictionary);
			} else if (line.split(":")[0].equalsIgnoreCase("DICTIONARY")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
				model.setInnerDictionary(null);
			//	System.out.println("empty dic");
			} else {
				System.err.println("read dic errors");
				System.exit(0);
			}
			// read an empty line
			reader.readLine(); // read an empty line
			line = reader.readLine();
			if (line.split(":")[0].equalsIgnoreCase("AVMAP")
					&& Integer.parseInt(line.split(":")[1]) > 1) {
				int dicSize = Integer.parseInt(line.split(":")[1]);
				HashMap<String, Pair<Integer, Integer>> amap = new HashMap();

				// read features
				for (int i = 0; i < dicSize; i++) {
					line = reader.readLine();
					String[] terms = line.split("\\s+");
					amap.put(terms[0], new Pair(Integer.parseInt(terms[1]),
							Integer.parseInt(terms[2])));
				}
				model.setAvMap(amap);
			} else if (line.split(":")[0].equalsIgnoreCase("AVMAP")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
			//	System.out.println("avmap empty");
			} else {
				System.err.println("read av feature errors");
				System.exit(0);
			}

			// read an empty line
			reader.readLine(); // read an empty line
			line = reader.readLine();
			if (line.split(":")[0].equalsIgnoreCase("BROWN")
					&& Integer.parseInt(line.split(":")[1]) > 1) {
				int dicSize = Integer.parseInt(line.split(":")[1]);
				HashMap<String, Pair<String, Integer>> brown = new HashMap();

				// read features
				for (int i = 0; i < dicSize; i++) {
					line = reader.readLine();
					String[] terms = line.split("\\s+");
					brown.put(terms[0],
							new Pair(terms[1], Integer.parseInt(terms[2])));
				}
				model.setBrownMap(brown);
			} else if (line.split(":")[0].equalsIgnoreCase("BROWN")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
			//	System.out.println("brown map empty");
			} else {
				System.err.println("read brown feature errors");
				System.exit(0);
			}

			reader.readLine(); // read an empty line
			line = reader.readLine();
			if (line.split(":")[0].equalsIgnoreCase("LDA")
					&& Integer.parseInt(line.split(":")[1]) > 1) {
				int dicSize = Integer.parseInt(line.split(":")[1]);
				HashMap<String, Vector<Double>> lda = new HashMap();

				// read features
				for (int i = 0; i < dicSize; i++) {
					line = reader.readLine();
					String[] terms = line.split("\\s+");
					Vector<Double> vec = new Vector();
					for (int j = 1; j < terms.length; j++) {
						vec.add(Double.parseDouble(terms[j]));
					}
					lda.put(terms[0], vec);
				}
				model.setLDAMap(lda);
			} else if (line.split(":")[0].equalsIgnoreCase("LDA")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
			//	System.out.println("lda empty");
			} else {
				System.err.println("read lda feature errors");
				System.exit(0);
			}

			reader.readLine(); // read an empty line
			line = reader.readLine();
			if (line.split(":")[0].equalsIgnoreCase("NNLM")
					&& Integer.parseInt(line.split(":")[1]) > 1) {
				int dicSize = Integer.parseInt(line.split(":")[1]);
				HashMap<String, Vector<Double>> nnlm = new HashMap();

				// read features
				for (int i = 0; i < dicSize; i++) {
					line = reader.readLine();
					String[] terms = line.split("\\s+");
					Vector<Double> vec = new Vector();
					for (int j = 1; j < terms.length; j++) {
						vec.add(Double.parseDouble(terms[j]));
					}
					nnlm.put(terms[0], vec);
				}
				model.setLDAMap(nnlm);
			} else if (line.split(":")[0].equalsIgnoreCase("NNLM")
					&& Integer.parseInt(line.split(":")[1]) < 1) {
				//System.out.println("nnlm empty");
			} else {
				System.err.println("read nnlm feature errors");
				System.exit(0);
			}

		} catch (IOException e) {
			System.err.println("can not read the model file, please check");
			System.exit(0);
		}
		return model;
	}

	private void shrinkFeatures() {
		Iterator<String> it = featureMap.keySet().iterator();
		// remove zero weight features
		while (it.hasNext()) {
			String feature = it.next();
			int fid = featureMap.get(feature);
			if (averageWeights[fid] == 0) {
				it.remove();
			}
		}
		System.out.println(featureMap.size());
	}

	public void saveModel(String filepath) {
		// if only model name, keep the file current dir
		if (filepath.indexOf("/") == -1) {
			filepath = "./" + filepath;
		}

		File model = new File(filepath);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(model));
			shrinkFeatures();
			Iterator<String> it = featureMap.keySet().iterator();
			// remove zero weight features
			writer.write("TYPE:" + type + "\n");
			writer.write("FEATURESIZE:" + featureMap.size() + "\n");
			while (it.hasNext()) {
				String feature = it.next();
				int fid = featureMap.get(feature);
				writer.write(feature + "\t" + averageWeights[fid] + "\n");
			}
			writer.flush();
			if (null != innerDictionary) {
				writer.write("\nDICTIONARY:" + innerDictionary.size() + "\n");
				it = innerDictionary.keySet().iterator();
				while (it.hasNext()) {
					String word = it.next();
					int freq = innerDictionary.get(word);
					writer.write(word + "\t" + freq + "\n");
				}
			} else {
				writer.write("\nDICTIONARY:0" + "\n");
			}

			if (null != avMap) {
				writer.write("\nAVMAP:" + avMap.size() + "\n");
				it = avMap.keySet().iterator();
				while (it.hasNext()) {
					String word = it.next();
					writer.write(word + "\t" + avMap.get(word).getKey() + "\t"
							+ avMap.get(word).getValue() + "\n");
				}
			} else {
				writer.write("\nAVMAP:0" + "\n");
			}

			if (null != brownMap) {
				writer.write("\nBROWN:" + brownMap.size() + "\n");
				it = brownMap.keySet().iterator();
				while (it.hasNext()) {
					String word = it.next();
					writer.write(word + "\t" + brownMap.get(word).getKey()
							+ "\t" + brownMap.get(word).getValue() + "\n");
				}
			} else {
				writer.write("\nBROWN:0" + "\n");
			}

			if (null != ldaMap) {
				writer.write("\nLDA:" + ldaMap.size() + "\n");
				it = ldaMap.keySet().iterator();
				while (it.hasNext()) {
					String word = it.next();
					Vector<Double> vec = ldaMap.get(word);
					writer.write(word + "\t");
					for (double v : vec) {
						writer.write(v + "\t");
					}
					writer.write("\n");

				}
			} else {
				writer.write("\nLDA:0" + "\n");
			}

			if (null != nnlmMap) {
				writer.write("\nNNLM:" + nnlmMap.size() + "\n");
				it = nnlmMap.keySet().iterator();
				while (it.hasNext()) {
					String word = it.next();
					Vector<Double> vec = nnlmMap.get(word);
					writer.write(word + "\t");
					for (double v : vec) {
						writer.write(v + "\t");
					}
					writer.write("\n");

				}
			} else {
				writer.write("\nNNLM:0" + "\n");
			}

			writer.close();

		} catch (IOException e) {
			System.err.println("can not save the model file, please check");
			System.exit(0);
		}

	}

}