package naist.cl.shmm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import naist.cl.util.CommandSetting;
import naist.cl.util.MyReader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class SemiMarkov {
	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.err
					.println("commandline parse error, please check the usage!!");
			printUage();
			System.exit(0);
		}

		Options opt = new Options();
		opt.addOption(CommandSetting.ACTION, true, "learning or segmentation");
		opt.addOption(CommandSetting.CORRECTRATE, true,
				"the correct rate above this, stop learning");
		opt.addOption(CommandSetting.DICTIONNARYFEATURE, true,
				"wether include the innerdictionary feature");
		opt.addOption(CommandSetting.MAXITERATION, true,
				"max iteration in learning stage");

		opt.addOption(CommandSetting.MODEL, true, "model file path");
		opt.addOption(CommandSetting.TRANININGFILE, true, "training file");
		opt.addOption(CommandSetting.OUPUT, true, "output file");
		opt.addOption(CommandSetting.FILTERFREQ, true, "feature filter freq");
		opt.addOption(CommandSetting.MAXLENGTH, true, "max length");
		opt.addOption(CommandSetting.INPUT, true, "input file to segment");
		opt.addOption(CommandSetting.HELP, true, "help");
		opt.addOption(CommandSetting.AV, true, "access variaty features");
		opt.addOption(CommandSetting.BROWN, true, "brown cluster features");
		opt.addOption(CommandSetting.LDA, true, "lda features");
		opt.addOption(CommandSetting.NNLM, true, "nnlm features");
		opt.addOption(CommandSetting.LDA_SCALAR, true, "lda scalar");
		opt.addOption(CommandSetting.NNLM_SCALAR, true, "nnlm scalar");
		opt.addOption(CommandSetting.CACHE, true, "cache");
		opt.addOption(CommandSetting.TYPE, true, "ctype");
		opt.addOption(CommandSetting.TOKEN_TYPE, true, "token type");

		CommandLineParser parser = new GnuParser();
		CommandLine commandLine;
		try {
			commandLine = parser.parse(opt, args);

			if (commandLine.hasOption(CommandSetting.HELP)) {
				printUage();
				System.exit(0);
			}
			if (commandLine.hasOption(CommandSetting.ACTION)) {
				String action = commandLine
						.getOptionValue(CommandSetting.ACTION);
				if (action.equalsIgnoreCase("learn")
						|| action.equalsIgnoreCase("l")
						|| action.equalsIgnoreCase("learning")) {
					boolean correct = true;
					Setting setting = new Setting();

					if (commandLine.hasOption(CommandSetting.TRANININGFILE))
						setting.tfile = commandLine
								.getOptionValue(CommandSetting.TRANININGFILE);
					else
						correct = false;
					if (commandLine.hasOption(CommandSetting.MODEL))
						setting.model = commandLine
								.getOptionValue(CommandSetting.MODEL);
					else
						correct = false;
					if (!correct) {
						System.err
								.println("commandline parse error, please check the usage!!");
						printUage();
					}

					if (commandLine.hasOption(CommandSetting.CORRECTRATE))
						setting.crate = Double.parseDouble(commandLine
								.getOptionValue(CommandSetting.CORRECTRATE));
					if (commandLine
							.hasOption(CommandSetting.DICTIONNARYFEATURE))
						setting.dicft = Boolean
								.parseBoolean(commandLine
										.getOptionValue(CommandSetting.DICTIONNARYFEATURE));

					if (commandLine.hasOption(CommandSetting.FILTERFREQ))
						setting.freq = Integer.parseInt(commandLine
								.getOptionValue(CommandSetting.FILTERFREQ));
					if (commandLine.hasOption(CommandSetting.MAXLENGTH))
						setting.maxLen = Integer.parseInt(commandLine
								.getOptionValue(CommandSetting.MAXLENGTH));
					if (commandLine.hasOption(CommandSetting.MAXITERATION))
						setting.maxIter = Integer.parseInt(commandLine
								.getOptionValue(CommandSetting.MAXITERATION));

					if (commandLine.hasOption(CommandSetting.AV))
						setting.av = commandLine
								.getOptionValue(CommandSetting.AV);

					if (commandLine.hasOption(CommandSetting.BROWN))
						setting.brown = commandLine
								.getOptionValue(CommandSetting.BROWN);

					if (commandLine.hasOption(CommandSetting.NNLM))
						setting.nnlm = commandLine
								.getOptionValue(CommandSetting.NNLM);

					if (commandLine.hasOption(CommandSetting.LDA))
						setting.lda = commandLine
								.getOptionValue(CommandSetting.LDA);

					if (commandLine.hasOption(CommandSetting.LDA_SCALAR))
						setting.lscalar = Double.parseDouble(commandLine
								.getOptionValue(CommandSetting.LDA_SCALAR));
					if (commandLine.hasOption(CommandSetting.NNLM_SCALAR))
						setting.nscalear = Double.parseDouble(commandLine
								.getOptionValue(CommandSetting.NNLM_SCALAR));
					if (commandLine.hasOption(CommandSetting.CACHE))
						setting.cache = Boolean.parseBoolean(commandLine
								.getOptionValue(CommandSetting.CACHE));

					if (commandLine.hasOption(CommandSetting.TYPE))
						setting.type = Boolean.parseBoolean(commandLine
								.getOptionValue(CommandSetting.TYPE));
				
					learn(setting);

				} else if (action.equalsIgnoreCase("segment")
						|| action.equalsIgnoreCase("s")
						|| action.equalsIgnoreCase("segmentation")) {
					String model = "";
					String input = "";
					String output = "";
					int maxLen = 7;
					boolean correct = true;
					if (commandLine.hasOption(CommandSetting.OUPUT))
						output = commandLine
								.getOptionValue(CommandSetting.OUPUT);
					else
						correct = false;
					if (commandLine.hasOption(CommandSetting.MODEL))
						model = commandLine
								.getOptionValue(CommandSetting.MODEL);
					else
						correct = false;

					if (commandLine.hasOption(CommandSetting.INPUT))
						input = commandLine
								.getOptionValue(CommandSetting.INPUT);
					else
						correct = false;
					if (!correct) {
						System.err
								.println("commandline parse error, please check the usage!!");
						printUage();
					}

					if (commandLine.hasOption(CommandSetting.MAXLENGTH))
						maxLen = Integer.parseInt(commandLine
								.getOptionValue(CommandSetting.MAXLENGTH));

					segmentFile(input, output, model, maxLen);

				} else {
					System.err
							.println("commandline parse error, please check the usage!!");
					printUage();
					System.exit(0);
				}
			} else {
				System.err
						.println("commandline parse error, please check the usage!!");
				printUage();
				System.exit(0);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			System.err
					.println("commandline parse error, please check the usage!!");
			printUage();
			System.exit(0);
		}

	}

	private static void printUage() {
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("uage:");
		System.out.println("java \n -classpath [*].jar shmm.SemiMarkov\n -"
				+ CommandSetting.ACTION
				+ " <action>(learning or segmentation)\n -"
				+ CommandSetting.CORRECTRATE + " <correctRate>\n -"
				+ CommandSetting.DICTIONNARYFEATURE + " <innerDictionary>\n -"
				+ CommandSetting.FILTERFREQ + " <filterFreq>\n -"
				+ CommandSetting.MAXITERATION + " <maxIteration>\n -"
				+ CommandSetting.MAXLENGTH + " <maxLength>\n -"
				+ CommandSetting.MODEL + " <model>\n -" + CommandSetting.OUPUT
				+ " <outputfile>\n -" + CommandSetting.INPUT + " <input>\n -"
				+ CommandSetting.TRANININGFILE + " <trainingFile>\n -"
				+ CommandSetting.AV + " <avfeature> \n -"
				+ CommandSetting.BROWN + " <brwon> \n -" + CommandSetting.LDA
				+ " <lda>\n -" + CommandSetting.NNLM + " <nnlm>\n");
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	//
	public static void segmentFile(String input, String output,
			String modelFile, int maxLen) {

		long begin = System.currentTimeMillis();
		Model model = Model.readModel(modelFile);
		System.out.println("loading model takes:"
				+ (System.currentTimeMillis() - begin) + " ms");
		begin = System.currentTimeMillis();
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(output));
			System.out.println("segment:" + output);
			MyReader reader = new MyReader(input);
			int counter = 0;
			String line;
			while (null != (line = reader.readLine())) {
				line = line.trim();
				counter++;
				StringBuffer buffer = new StringBuffer();
				if (line.isEmpty()) {
					writer.write("\n");
					continue;
				}
				Lattice ltt = new Lattice(line, model, maxLen, false);
				LinkedList<Node> best = ltt.decoding(model);
				for (Node node : best) {
					buffer.append(line.subSequence(node.getBegin(),
							node.getBegin() + node.getLength())
							+ " ");
				}
				buffer.append("\n");
				writer.write(buffer.toString());
			}
			writer.flush();
			writer.close();
			// System.out.println(buffer.toString());
			System.out.println("it takes about: "
					+ (System.currentTimeMillis() - begin) + "ms segment "
					+ counter + " sentence including reading file time");
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String segmentSentence(String sentence, String modelFile,
			int maxLen) {
		Model model = Model.readModel(modelFile);
		Lattice ltt = new Lattice(sentence, model, maxLen, false);
		LinkedList<Node> best = ltt.decoding(model);
		StringBuffer buffer = new StringBuffer();
		for (Node node : best) {
			buffer.append(sentence.subSequence(node.getBegin(), node.getBegin()
					+ node.getLength())
					+ " ");
		}
		return buffer.toString();
	}

	public static Model learn(Setting setting) throws IOException {
		long begin = System.currentTimeMillis();
		LinkedList<Lattice> trainingInstances = new LinkedList();

		System.out
				.println("training configuration:\n[~~~~~~~~~~~~~~~~~~~~\ntraining file: "
						+ setting.tfile + "\nmodel file:" + setting.model);
		System.out.println("dicFeature: " + setting.dicft + "\nmaxLen:"
				+ setting.maxLen + "\nmaxIter:" + setting.maxIter
				+ "\ncorrect rate:" + setting.crate + "\nfeatureFilter freq:"
				+ setting.freq + "\n");
		System.out.println("~~~~~~~~~~~~~~~~~~~~]");
		System.out.println("begin to extract features...");

		Model model = new Model();
		if (setting.dicft) {
			HashMap<String, Integer> innerDictionary = null;
			innerDictionary = FeatureExtractor
					.ditcitonaryExtract(setting.tfile);
			model.setInnerDictionary(innerDictionary);
		}
		if (!setting.av.isEmpty()) {
			model.setAvMap(MyReader.parseAv(setting.av));
		}
		if (!setting.brown.isEmpty()) {
			model.setBrownMap(MyReader.parseBrown(setting.brown));
		}
		if (!setting.lda.isEmpty()) {
			model.setLDAMap(MyReader.parseLDA(setting.lda));
		}
		if (!setting.nnlm.isEmpty()) {
			model.setNNLMMap(MyReader.parseLDA(setting.nnlm));
		}

		model.setType(setting.type);

		FeatureExtractor.featuresExtractor(model, setting);

		System.out.println("Extracting features took around "
				+ (System.currentTimeMillis() - begin) / 1000 + "s");
		begin = System.currentTimeMillis();
		System.out.println("feature size:" + model.getFeatureMap().size());
		// System.out.println("init model ...");
		//
		String line;
		System.out.println("Init model took around "
				+ (System.currentTimeMillis() - begin) / 1000 + "s");
		begin = System.currentTimeMillis();
		// /create lattice
		MyReader reader = new MyReader(setting.tfile);
		ArrayList<String> tdata = new ArrayList<String>();
		int numInstance = 0;
		while (null != (line = reader.readLine())) {
			if (line.trim().isEmpty())
				continue;
			if (setting.cache)
				trainingInstances.add(new Lattice(line, model, setting.maxLen,
						true));
			else
				tdata.add(line);
			numInstance++;
		}
		reader.close();
		System.out.println("Init training instances took around "
				+ (System.currentTimeMillis() - begin) / 1000 + "s");
		begin = System.currentTimeMillis();
		System.out.println("trianing instances:" + numInstance);
		double trate = 0;
		int counter = 0;
		while (trate < setting.crate && counter < setting.maxIter) {
			double correct = 0;
			double total = 0;
			counter++;
			long inbegin = System.currentTimeMillis();
			for (int i = 0; i < numInstance; i++) {
				if (i % 500 == 0) {
					System.out.println("training instances " + i + " -> "
							+ (i + 500) + " ...");
				}
				Lattice ltt;
				if (setting.cache)
					ltt = trainingInstances.get(i);
				else
					ltt = new Lattice(tdata.get(i), model, setting.maxLen, true);
				LinkedList<Node> best = ltt.decoding(model);
				LinkedList<Node> gold = ltt.getGoldList();
				update(gold, best, model, counter);
				correct += correctCount(gold, best);
				total += ltt.getNumWord();
			}
			trate = correct / total;
			System.out.println("iteration: " + counter + " with correct rate:"
					+ trate + " and took around "
					+ (System.currentTimeMillis() - inbegin) / 1000 + "s");
		}

		model.modelWeightUpdate(counter);
		model.saveModel(setting.model);
		System.out.println((System.currentTimeMillis() - begin) / 1000 + "s");

		return null;
	}

	private static int correctCount(LinkedList<Node> goldList,
			LinkedList<Node> bestpath) {
		int count = 0;
		for (int i = 0; i < bestpath.size(); i++) {
			for (int j = 0; j < goldList.size(); j++) {
				if (bestpath.get(i) == goldList.get(j)) {
					count++;
				}
			}
		}
		return count;
	}

	private static void print(String sentence, LinkedList<Node> list) {
		for (int i = 0; i < list.size(); i++) {
			Node n = list.get(i);
			System.out.print(sentence.substring(n.getBegin(),
					n.getBegin() + n.getLength())
					+ " ");
		}
	}

	private static void update(LinkedList<Node> goldList,
			LinkedList<Node> bestpath, Model model, int c) {
		for (int i = 0; i < bestpath.size(); i++) {
			Node n = bestpath.get(i);
			int[] fvec = n.getFeatureVector();
			for (int j = 0; j < fvec.length; j++) {
				if (null != n.getCWeight()
						&& n.getCWeight().containsKey(fvec[j])) {
					model.updateWeight(fvec[j], -n.getCWeight().get(fvec[j]));
					model.updateAveWeight(fvec[j],
							-c * n.getCWeight().get(fvec[j]));
				} else {
					model.updateWeight(fvec[j], -1);
					model.updateAveWeight(fvec[j], -c);
				}
			}
		}
		for (int i = 0; i < goldList.size(); i++) {

			int[] fvec = goldList.get(i).getFeatureVector();
			Node n = goldList.get(i);
			for (int j = 0; j < fvec.length; j++) {
				if (null != n.getCWeight()
						&& n.getCWeight().containsKey(fvec[j])) {
					model.updateWeight(fvec[j], n.getCWeight().get(fvec[j]));
					model.updateAveWeight(fvec[j],
							c * n.getCWeight().get(fvec[j]));
				} else {
					model.updateWeight(fvec[j], 1);
					model.updateAveWeight(fvec[j], c);
				}
			}
		}

	}
}