package naist.cl.util;

import java.io.Serializable;

/**
 * @author allen
 * 
 * */
public class Pair<K, V> implements Serializable {
	private K key;
	private V value;

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public Pair(K key) {
		this(key, null);
	}

	public Pair() {
		this(null, null);
	}

	public void setKey(K key) {
		this.key = key;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public String toString() {
		String rst = "";
		rst = "key:" + key + "\t" + value;
		return rst;
	}

}