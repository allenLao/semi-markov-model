package naist.cl.shmm;

//default setting 
public class Setting {
	public String model = "";
	public String tfile = "";
	public String nnlm = "";
	public String av = "";
	public String brown = "";
	public String lda = "";
	public double crate = 0.99;
	public double lscalar = 1;
	public double nscalear = 1;
	public int maxIter = 20;
	public int freq = 0;
	public int maxLen = 7;
	public boolean dicft = false;
	public boolean cache = true;
	public boolean type = false;
}