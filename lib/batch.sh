#training
java -classpath ./lib/shmm.jar -action learn  -model pku_model -tfile ./sighan/train -maxLen 9 -crate 0.99 -dicFt false -freq 2 -maxIter 100
#segmentation  
java -classpath ./lib/shmm.jar -action segment -model pku_model -input ./sighan/pku_test.utf8 -output ./result -maxLen 9 
