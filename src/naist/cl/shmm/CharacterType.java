package naist.cl.shmm;

public class CharacterType {
	public static void main(String[] args) {
		System.out.println(CharacterType.englishFull2Half("Ｂ"));
	}

	public static int type(String c) {
		int rst = -1;
		if (isEnglish(c))
			rst = 0;
		else if (isNumber(c))
			rst = 1;
		else if (isPunctuation(c))
			rst = 2;
		else if (isChineseFamliyName(c) || isForeignName(c))
			rst = 3;
		else if (isPostfix(c))
			rst = 4;
		return rst;
	}

	public static int StringType(String str) {
		if (isEnglishNumberSeq(str))
			return 0;
		if (isEnglishNumberPuncSeq(str))
			return 1;
		if (isChineseSpectialFN(str))
			return 3;
		return -1;
	}

	public static String convert2Half(String sentence) {
		if (null == sentence || sentence.isEmpty()) {
			return null;
		}
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < sentence.length(); i++) {
			if (isEnglish(sentence.charAt(i) + "")) {
				buffer.append(englishFull2Half(sentence.charAt(i) + ""));
			} else if (isNumber(sentence.charAt(i) + "")) {
				buffer.append(numberFull2Half(sentence.charAt(i) + ""));
			} else {
				buffer.append(sentence.charAt(i));
			}
		}
		return buffer.toString();
	}

	public static boolean isEnglishNumberSeq(String str) {
		boolean rst = true;
		if (str.length() < 2)
			return false;
		for (int i = 0; i < str.length(); i++) {
			if (!isEnglish(str.charAt(i) + "") || !isNumber(str.charAt(i) + "")) {
				rst = false;
				break;
			}
		}
		return rst;
	}

	public static boolean isEnglishNumberPuncSeq(String str) {
		boolean rst = true;
		if (str.length() < 2)
			return false;
		for (int i = 0; i < str.length(); i++) {
			if (!isEnglish(str.charAt(i) + "") || !isNumber(str.charAt(i) + "")
					|| !isPunctuation(str.charAt(i) + "")) {
				rst = false;
				break;
			}
		}
		return rst;
	}

	public static boolean isEnglish(String c) {
		return FULL_ENGLISH.contains(c) || HALF_ENGLISH.contains(c);
	}

	public static String englishFull2Half(String c) {
		if (FULL_ENGLISH.contains(c))
			return HALF_ENGLISH.charAt(FULL_ENGLISH.indexOf(c)) + "";
		return c;
	}

	public static boolean isNumber(String c) {
		return FULL_NUMBER.contains(c) || HALF_NUMBER.contains(c)
				|| CHINESE_NUMBER.contains(c);
	}

	public static String numberFull2Half(String c) {
		if (FULL_NUMBER.contains(c))
			return HALF_NUMBER.charAt(FULL_NUMBER.indexOf(c)) + "";
		return c;
	}

	public static boolean isPunctuation(String word) {
		return PUNCTUATION.contains(word);
	}

	public static boolean isChineseFamliyName(String c) {
		return CHINESE_FAMILY_NAME.contains(c);
	}

	public static boolean isForeignName(String c) {
		return TRANS_ENGLISH.contains(c) || TRANS_JAPANESE.contains(c)
				|| TRANS_RUSSIAN.contains(c);
	}

	/*
	 * only two character
	 */
	public static boolean isChineseSpectialFN(String word) {
		boolean rst = false;
		for (int i = 0; i < CHINESE_BIFAMILY_NAME.length; i++)
			if (CHINESE_BIFAMILY_NAME[i].equals(word)) {
				rst = true;
				break;
			}
		return rst;
	}

	public static boolean isPostStr(String words) {
		for (String pos : POSTFIX_MUTIPLE) {
			if (pos.equals(words))
				return true;
		}
		return false;
	}

	public static boolean isPostfix(String c) {
		if (POSTFIX_SINGLE.contains(c))
			return true;
		return false;
	}

	public static final String STRANGE = "≠ ≡ ＃ ≤ ┝ ≥ ├ ┟ ＆ ┞ ┑ ┐ ＋ ┓ ┒ － ／ ≮ ≯ ┈"
			+ " ┊ ┌ ┍ ┎ ┏ ─ │ ＜ ┄ ＝ ＞ ┆ ＠ ┿ ┾ ┽ ┼ ☆ ┳ ┲ ≈ ┱ ★ ┰ ≌ ┮ ┯ ┬ ┭ ┢"
			+ " ┣ ┠ ┡ ＾ ＿ ＼ ∥ ∧ ∠ № ∮ ∨ ∩ ∪ ∫ ∵ ∴ ∷ ∽ ╀ ╁ ╂ ╃ ⌒ ∏ ∈ 〓"
			+ " ∑ ∞ ∝ ※ √ ▲ △ © ® § □ ■ ± ← ⊥ ↑ → ↓ ◇ ◆ ● × ◎ ○ ⊙ ÷";

	public static final String FULL_NUMBER = "０１２３４５６７８９";
	public static final String HALF_NUMBER = "0123456789";
	public static final String FULL_ENGLISH = "ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ";
	public static final String HALF_ENGLISH = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static final String PUNCTUATION = "！ ＂ ＇ ） （ ， ． ： ； ？ \" ! ¨ & ' * ( ) . , - ; : ［ > · < ］ @ ｀ ∶ ‖ ]"
			+ " ｜ \\ ｝ ～ ^ ‘ [ ” “ ｛ ˉ 〃 。 、 … 》 《 〉 〈 』 『 ˇ 」 「 【 】 〖 〗 〔 〕 ~ } | { ";
	public static final String CHINESE_NUMBER = "零〇一二两三四五六七八九十廿百千万亿壹贰叁肆伍陆柒捌玖拾佰仟年月日时分秒∶・．／点毫厘顷钱两斤担";

	public static final String POSTFIX_SINGLE = "坝邦堡杯城池村单岛道堤店洞渡队法峰府冈港阁宫沟国海号河湖环集江奖"
			+ "礁角街井郡坑口矿里岭楼路门盟庙弄牌派坡铺旗桥区渠泉人山省市水寺塔台滩坛堂厅亭屯湾文屋溪峡县线乡巷型洋窑营屿语园苑院闸寨站镇州庄族陂庵町";

	public static final String[] POSTFIX_MUTIPLE = { "半岛", "草原", "城市", "大堤",
			"大公国", "大桥", "地区", "帝国", "渡槽", "港口", "高速公路", "高原", "公路", "公园",
			"共和国", "谷地", "广场", "国道", "海峡", "胡同", "机场", "集镇", "教区", "街道", "口岸",
			"码头", "煤矿", "牧场", "农场", "盆地", "平原", "丘陵", "群岛", "沙漠", "沙洲", "山脉",
			"山丘", "水库", "隧道", "特区", "铁路", "新村", "雪峰", "盐场", "盐湖", "渔场", "直辖市",
			"自治区", "自治县", "自治州", };

	public static final String[] CHINESE_BIFAMILY_NAME = { "万俟", "司马", "上官",
			"欧阳", "夏侯", "诸葛", "闻人", "东方", "赫连", "皇甫", "尉迟", "公羊", "澹台", "濮阳",
			"淳于", "单于", "太叔", "申屠", "公孙", "仲孙", "轩辕", "令狐", "钟离", "宇文", "长孙",
			"慕容", "司徒", "司空", "黄辰", "纳喇", "乌雅", "范姜", "碧鲁", "张廖", "太史", "公叔",
			"乌孙", "完颜", "富察", "费莫", "第五", "南宫", "西门", "东门", "左丘", "梁丘", "微生",
			"羊舌", "呼延", "南门", "东郭", "百里", "谷梁", "宰父", "夹谷", "拓跋", "壤驷", "乐正",
			"漆雕", "公西", "巫马", "端木", "颛孙", "子车", "司寇", "亓官", "三小", "鲜于", "锺离",
			"闾丘", "公良", "段干" };

	public static final String CHINESE_FAMILY_NAME = "乾大桑桐原黑天冲迦太头氏筑筒城"
			+ "渡访冢乙永水形橘外渊进桥冈远筱久内近多茂影神清" + "樱栗越丹荻丸出奥江见高丰玉达女角及友边又子"
			+ "辻中诹曾横樫好置矶崎笠麻石东司荒凑飞喜奈叶根阪三上" + "草下垣矢熊古池樋阿风笹足汤口那守安仲町北工真沟儿秦甲春宇"
			+ "美田轮宅志照妹代坂赤雨生宫秋明所河仓沼恩室鹿手铃星坪鹤" + "森藤川羽家今南富关土须鸟部入千十户智都八泉梅畠半场福五井檟泷"
			+ "臼地畑梶泽唐贯元难我西波成对梨菅德马寺猪菊贺二武青皆镰斐正老山长"
			+ "堤船袋葛斋品广楠狩新林并加施园津枝平方屋洼首堀斯国香居幡佐尾"
			+ "柴尻良柳伯植椎米益浅椋龟浜盐布市伊染和目萩稻酒浦会小砂锅日"
			+ "柏早海相落片芦峰分肉间末木门芳立本花味股纳望牧增朝竹利涩鬼月有槻"
			+ "端服毛比保境板嘉饭松结胁若榊前岸细织庄绀胜条岩名后吉谷合金爪村岛岚持"
			+ "白百深杉濑野添吕里重向绪依庭赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华"
			+ "金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤滕殷罗"
			+ "毕郝邬安常乐于时傅皮卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计伏成戴谈宋茅庞熊纪舒屈"
			+ "项祝董梁杜阮蓝闵席季麻强贾路娄危江童颜郭梅盛林刁锺徐邱骆高夏蔡田樊胡凌霍虞万支柯昝管卢莫经房裘缪"
			+ "干解应宗丁宣贲邓郁单杭洪包诸左石崔吉钮龚程嵇邢滑裴陆荣翁荀羊於惠甄麴"
			+ "家封芮羿储靳汲邴糜松井段富巫乌焦巴弓牧隗山谷车侯宓蓬全郗班仰秋仲伊宫宁仇栾暴"
			+ "甘钭历戎祖武符刘景詹束龙叶幸司韶郜黎蓟溥印宿白怀蒲邰从鄂索咸籍赖卓蔺屠蒙池乔阳郁"
			+ "胥能苍双闻莘党翟谭贡劳逄姬申扶堵冉宰郦雍却璩桑桂濮牛寿通边扈燕冀僪浦尚农温别庄晏柴瞿"
			+ "阎充慕连茹习宦艾鱼容向古易慎戈廖庾终暨居衡步都耿满弘匡国文寇广禄阙东欧殳沃利蔚越夔隆师巩"
			+ "厍聂晁勾敖融冷訾辛阚那简饶空曾毋沙乜养鞠须丰巢关蒯相查后荆红游竺权逮盍益桓公召有舜岳寸贰皇"
			+ "侨彤竭端赫实甫集象翠狂辟典良函芒苦其京中夕之冠宾香果蹇称诺来多繁戊朴回毓鉏税荤靖绪愈硕牢买但巧枚撒泰"
			+ "秘亥绍以壬森斋释奕姒朋求羽用占真穰翦闾漆贵代贯旁崇栋告休褒谏锐皋闳在歧禾示是委钊频嬴呼大威昂律冒保系抄"
			+ "化莱校么抗祢綦悟宏功庚务敏捷拱兆丑丙畅苟随类卯俟友答乙允甲留尾佼玄乘裔延植环矫赛昔侍度旷遇偶前由咎塞敛受"
			+ "泷袭衅叔圣御夫仆镇藩邸府掌首员焉戏可智尔凭悉进笃厚仁业肇资合仍九衷哀刑俎仵圭夷徭蛮汗孛乾帖罕洛淦洋邶"
			+ "郸郯邗邛剑虢隋蒿茆菅苌树桐锁钟机盘铎斛玉线针箕庹绳磨蒉瓮弭刀疏牵浑恽势世仝同蚁止戢睢冼种涂肖己泣潜卷脱"
			+ "谬蹉赧浮顿说次错念夙斯完丹表聊源姓吾寻展出不户闭才无书学愚本性雪霜烟寒少字桥板斐独千诗嘉扬善揭祈析赤紫青柔"
			+ "刚奇拜佛陀弥阿素长僧隐仙隽宇祭酒淡塔琦闪始星南天接波碧速禚腾潮镜似澄潭謇纵渠奈风春濯沐茂英兰檀"
			+ "藤枝检生折登驹骑貊虎肥鹿雀野禽飞节宜鲜粟栗豆帛官布衣藏宝钞银门盈庆喜及普建营巨望希道载声"
			+ "漫犁力贸勤革改兴亓睦修信闽北守坚勇汉练尉士旅五令将旗军行奉敬恭仪母堂丘义礼慈孝理伦卿问永辉"
			+ "位让尧依犹介承市所苑杞剧第零谌招续达忻六鄞战迟候宛励粘萨邝覃辜初楼城区局台原考妫纳泉老清德"
			+ "卑过麦曲竹百福言佟爱年笪谯哈墨连赏伯佴佘牟商琴后况亢缑帅海归钦鄢汝法闫楚晋督"
			+ "仉盖逯库郏逢阴薄厉稽开光操瑞眭泥运摩伟铁迮";

	public static final String TRANS_ENGLISH = "・―阿埃艾爱安昂敖奥澳笆芭巴白拜班邦保堡鲍北贝本比毕彼别波"
			+ "玻博勃伯泊卜布才采仓查差柴彻川茨慈次达大戴代丹旦但当道德得的登迪狄蒂帝丁东杜敦多额俄厄鄂恩尔伐法范菲芬费佛夫"
			+ "福弗甫噶盖干冈哥戈革葛格各根古瓜哈海罕翰汗汉豪合河赫亨侯呼胡华霍基吉及加贾坚简杰金京久居君喀卡凯坎康考柯科可克"
			+ "肯库奎拉喇莱来兰郎朗劳勒雷累楞黎理李里莉丽历利立力连廉良列烈林隆卢虏鲁路伦仑罗洛玛马买麦迈曼茅茂梅门蒙盟米蜜"
			+ "密敏明摩莫墨默姆木穆那娜纳乃奈南内尼年涅宁纽努诺欧帕潘畔庞培佩彭皮平泼普其契恰强乔切钦沁泉让热荣肉儒瑞若萨塞赛桑瑟"
			+ "森莎沙山善绍舍圣施诗石什史士守斯司丝苏素索塔泰坦汤唐陶特提汀图土吐托陀瓦万王旺威韦维魏温文翁沃乌吾武伍西锡希喜夏相香歇谢"
			+ "辛新牙雅亚彦尧叶依伊衣宜义因音英雍尤于约宰泽增詹珍治中仲朱诸卓孜祖佐伽娅尕腓滕济嘉津赖莲琳律略慕妮聂裴浦奇齐琴茹珊卫欣逊"
			+ "札哲智兹芙汶迦珀琪梵斐胥黛";

	public static final String TRANS_RUSSIAN = "・阿安奥巴比彼波布察茨大德得丁杜尔法夫伏甫盖格哈基加坚捷金卡科可克库拉莱"
			+ "兰勒雷里历利连列卢鲁罗洛马梅蒙米姆娜涅宁诺帕泼普奇齐乔切日萨色山申什斯索塔坦特托娃维文乌西希谢亚耶叶依伊以扎佐柴达"
			+ "登蒂戈果海赫华霍吉季津柯理琳玛曼穆纳尼契钦丘桑沙舍泰图瓦万雅卓兹";

	public static final String TRANS_JAPANESE = "安奥八白百邦保北倍本比滨博步部彩菜仓昌长朝池赤川船淳次村大代岛稻道德地"
			+ "典渡尔繁饭风福冈高工宫古谷关广桂贵好浩和合河黑横恒宏后户荒绘吉纪佳加见健江介金今进井静敬靖久酒菊俊康可克口梨理里礼栗"
			+ "丽利立凉良林玲铃柳隆鹿麻玛美萌弥敏木纳南男内鸟宁朋片平崎齐千前浅桥琴青清庆秋丘曲泉仁忍日荣若三森纱杉山善上伸神圣石实矢"
			+ "世市室水顺司松泰桃藤天田土万望尾未文武五舞西细夏宪相小孝新星行雄秀雅亚岩杨洋阳遥野也叶一伊衣逸义益樱永由有佑宇羽郁渊元垣"
			+ "原远月悦早造则泽增扎宅章昭沼真政枝知之植智治中忠仲竹助椎子佐阪坂堀荻菅薰浜濑鸠筱";

	public static final String CHINESE_YEAR_PREIX = "甲乙丙丁戊己庚辛壬癸";
	public static final String CHINESE_YEAR_SURFIX = "子丑寅卯辰巳午未申酉戌亥";

}